/**
 * @author haina.z@163.com
 * @brief math library
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "math_lib.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Static function declarations (prototypes) --------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

/**
 * @brief 去掉最大值最小值，再求平均值
 * 
 * @param pObj 对象
 * @param u16New 新数值
 * @param u8Num 总个数
 * @return true 已更新平均值
 * @return false 
 */
bool Math_Average1Apply(Avg1Word_t *pObj, uint16_t u16New, uint8_t u8Num)
{
    if (pObj->u8Cnt == 0)
    {
        pObj->u16Min = u16New;
        pObj->u16Max = u16New;
        pObj->u32Sum = u16New;
    }
    else
    {
        if (pObj->u16Min > u16New)
        {
            pObj->u16Min = u16New;
        }
        if (pObj->u16Max < u16New)
        {
            pObj->u16Max = u16New;
        }

        pObj->u32Sum += u16New;
    }

    pObj->u8Cnt++;
    if (pObj->u8Cnt >= u8Num)
    {
        if (pObj->u8Cnt > 2)
        {
            pObj->u32Sum -= pObj->u16Min;
            pObj->u32Sum -= pObj->u16Max;
            pObj->u8Cnt -= 2;
        }

        pObj->u16Avg = pObj->u32Sum / pObj->u8Cnt;
        pObj->u8Cnt = 0;

        return true;
    }

    return false;
}

/******************************** End of file *********************************/
