/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __MATH_H
#define __MATH_H

/** Includes -----------------------------------------------------------------*/

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/
// Get minimum value and get maximum value
#define mMin(a, b) (((a) < (b)) ? (a) : (b))
#define mMax(a, b) (((a) > (b)) ? (a) : (b))

// Number of members
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // MATH
/******************************** End of file *********************************/
