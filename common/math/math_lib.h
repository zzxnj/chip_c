/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __MATH_LIB_H
#define __MATH_LIB_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
typedef struct
{
    uint8_t u8Cnt;
    uint16_t u16Min;
    uint16_t u16Max;
    uint32_t u32Sum;
    uint16_t u16Avg;
} Avg1Word_t;

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/**
 * @brief 去掉最大值最小值，再求平均值
 * 
 * @param pObj 对象
 * @param u16New 新数值
 * @param u8Num 总个数，大于2个时，去掉最大最小值求平均值，否则直接计算平均值
 * @return true 已更新平均值
 * @return false 
 */
bool Math_Average1Apply(Avg1Word_t *pObj, uint16_t u16New, uint8_t u8Num);

/** Macros (#define) ---------------------------------------------------------*/
// Avg1Word_t type
#define mMath_Avg1Init(pObj)                    {(pObj)->u8Cnt = 0; (pObj)->u16Avg = 0;}
#define mMath_GetAvg1Value(pObj)                ((pObj)->u16Avg) // Get average value

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // MATH_LIB
/******************************** End of file *********************************/
