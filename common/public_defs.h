
/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __PUBLIC_DEFS_H
#define __PUBLIC_DEFS_H

/** Includes -----------------------------------------------------------------*/
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/
/* Boolean type definitions */
#if (defined(__cplusplus) || defined(__bool_true_false_are_defined))
typedef bool bool_t;
#define TRUE true
#define FALSE false
#else
typedef uint8_t bool_t;
#define TRUE 1
#define FALSE 0
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

/* Option/Active/Enable definitions */
#define dOptionOn (1u)
#define dOptionOff (0u)

#define dActive (0x55)
#define dDeactive (0x00)

#define dEnable (0xAA)
#define dDisable (0x00)

/* Macro definitions related to data types */
/* Bit mask */
#define mBitMask(offset) (0x1ul << (offset))

#define bmBit0 mBitMask(0)
#define bmBit1 mBitMask(1)
#define bmBit2 mBitMask(2)
#define bmBit3 mBitMask(3)
#define bmBit4 mBitMask(4)
#define bmBit5 mBitMask(5)
#define bmBit6 mBitMask(6)
#define bmBit7 mBitMask(7)
#define bmBit8 mBitMask(8)
#define bmBit9 mBitMask(9)
#define bmBit10 mBitMask(10)
#define bmBit11 mBitMask(11)
#define bmBit12 mBitMask(12)
#define bmBit13 mBitMask(13)
#define bmBit14 mBitMask(14)
#define bmBit15 mBitMask(15)
#define bmBit16 mBitMask(16)
#define bmBit17 mBitMask(17)
#define bmBit18 mBitMask(18)
#define bmBit19 mBitMask(19)
#define bmBit20 mBitMask(20)
#define bmBit21 mBitMask(21)
#define bmBit22 mBitMask(22)
#define bmBit23 mBitMask(23)
#define bmBit24 mBitMask(24)
#define bmBit25 mBitMask(25)
#define bmBit26 mBitMask(26)
#define bmBit27 mBitMask(27)
#define bmBit28 mBitMask(28)
#define bmBit29 mBitMask(29)
#define bmBit30 mBitMask(30)
#define bmBit31 mBitMask(31)

/* Bits number */
#define BITS_PER_BYTE (8u) // 每个Byte的位数
#define BITS_PER_WORD (16u) // 每个Word的位数
#define BITS_PER_LONG (32u) // 每个Long的位数

/* Get byte with offset */
#define mGetOffset0Byte(ptr) (*((uint8_t *)(ptr) + 0))
#define mGetOffset1Byte(ptr) (*((uint8_t *)(ptr) + 1))
#define mGetOffset2Byte(ptr) (*((uint8_t *)(ptr) + 2))
#define mGetOffset3Byte(ptr) (*((uint8_t *)(ptr) + 3))

/** Type definitions ---------------------------------------------------------*/
// Align 4bytes
typedef union
{
    uint32_t u32Data;
    uint16_t u16Data[2];
    uint8_t u8Data[4];
} DataAlign4Bytes_t;

// Function definition
typedef void (*pfnVoid)(void);

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/
#define xUnwrap(x) (x)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // PUBLIC_DEFS
/******************************** End of file *********************************/
