
/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __XSTRING_H
#define __XSTRING_H

/** Includes -----------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/
#define XSTRING_FAIL ((int16_t)-1)

#define xString_Wrap(s) #s // Not to use outside this file!
#define TO_STRING(s) xString_Wrap(s)

/* Uppercase <-> Lowercase */
#define TO_LOWER_CHAR(c) ((((c) >= 'A') && ((c) <= 'Z')) ? ((c) | 0x20) : (c))
#define TO_UPPER_CHAR(c) ((((c) >= 'a') && ((c) <= 'z')) ? ((c) & ~0x20) : (c))

/* Nibble (0x0 ~ 0xF) <-> char, Ascii code display */
#define NIBBLE_TO_CHAR(d) (((d) < 10) ? ((d) + '0') : (((d) < 16) ? ((d) + 'A' - 10) : ' '))
#define CHAR_TO_NIBBLE(c) ((((c) >= '0') && ((c) <= '9')) ? ((c) - '0') : (((c) >= 'a') && ((c) <= 'f')) ? ((c) - 'a' + 10) : (((c) >= 'A') && ((c) <= 'F')) ? ((c) - 'A' + 10)  : ' ')

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/**
 * @brief Search Key, return first matched
 * 
 * @param sSource Source string
 * @param sKey Key string
 * @return int16_t: >=0 Matched position; =-1 Matching failed
 */
int16_t xString_SearchKey(const char *sSource, const char *sKey);

/**
 * @brief String to hex value
 * 
 * @param sString Source string
 * @param pBuf Buffer userd for value
 * @param u16BufSize Buffer size
 * @return int16_t: >=0 Value size; =-1 Convert failed
 */
int16_t xString_StringToHex(const char *sString, uint8_t *pBuf, uint16_t u16BufSize);

/**
 * @brief Hex string to value
 * 
 * @param sString Source string
 * @param pBuf Buffer userd for value
 * @param u8Number The number of characters used for conversion
 * @return true: Convert success
 * @return false: Convert failed
 */
bool xString_HexStringToValue(const char *sString, uint8_t *pBuf, uint8_t u8Number);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // XSTRING
/******************************** End of file *********************************/
