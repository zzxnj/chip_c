# Chip C

#### 介绍
芯片C语言框架和实用程序集合<br>
交流加QQ群：1054201295

#### 目录结构
```
├─bsp                           > 板级总支持包
│  └─201                        > 工业加湿器电路板，用于调试验证
│
├─common                        > 公共的函数和定义
│  ├─math                       > 数学计算相关
│  ├─xstring                    > 字符串相关
│  └─public_defs.h              > 公共定义的头文件
│
├─csp                           > 芯片级支持包
│  ├─apm32f10x                  > APM32F10x 芯片相关
│  └─cortex_m                   > Cortex-Mx 内核相关
│
├─Libraries                     > 芯片厂家提供的外设库
│  ├─APM32F10x_ETH_Driver
│  ├─APM32F10x_StdPeriphDriver
│  ├─CMSIS
│  │  └─Include_Geehy
│  └─Device
│      └─Geehy                  > 极海半导体
│          └─APM32F10x
│
├─modules                       > 公共的模块
│  ├─algorithm                  > 算法
│  │  └─des                     > DES加密算法
│  ├─bit_access                 > 位操作
│  ├─communication              > 通讯协议
│  ├─crc                        > CRC相关
│  ├─digital                    > 数字信号
│  ├─filter                     > 滤波相关
│  └─time                       > 时间相关
│
├─Projects
│   └─201_humidifier            > 用于调试验证的项目
│       ├─application
│       ├─board                 > 匹配项目的板级信息
│       │  └─test               > 用于验证芯片功能的代码
│       ├─Include
│       ├─Project
│       ├─Source
│       └─prj_config.h          > 项目配置文件
└─test                          > 用于验证功能的代码

```

#### 其他説明
keil arm 编译器，使用版本 MDK527

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
