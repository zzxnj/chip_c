#include "public_defs.h"

#include "test_math.h"
#include "test_xstring.h"

#include "test_bit.h"
#include "test_crc.h"
#include "test_des.h"
#include "test_filter.h"

int main(int argc, char *argv[]) {
    // 回显参数
    for (int i = 0; i < argc; i++)
    {
        puts(argv[i]);
    }

    // common验证
    TestMath_Perform();
    TestXString_Perform();

    // modules验证
    TestBit_Perform();
    TestCrc_Perform();
    TestDes_Perform();
    TestFilter_Perform();

    system("pause");
    return 0;
}
