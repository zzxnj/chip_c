/**
 * @author haina.z@163.com
 * @brief config stored in flash
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "time/time_flag.h"
#include "store/store_flash.h"
#include "./flash_apl.h"

/** Macros and constants -----------------------------------------------------*/
#define mFlashConfig_DataList() \
    mFlashData_Def(Config01,    &u8Config1,                     0xFF) \
    mFlashData_Def(Config02,    &mLow(u16Config2),              0xFF) \
    mFlashData_Def(Config03,    &mHigh(u16Config2),             0xFF) \

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static uint8_t u8Config1;
static uint16_t u16Config2;

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/
#define mFlashData_Def(name, address, mask) {(uint8_t *)(address), (mask)},
mStoreFlash_Create(Config, \
                    FLASH_CONFIG_START_ADDR, FLASH_CONFIG_END_ADDR, \
                    FLASH_PAGE_SIZE, FLASH_CONFIG_BLOCKS, \
                    FLASH_ALIGN_SIZE, FLASH_TYPE_CONFIG, \
                    mFlashConfig_DataList());
#undef mFlashData_Def

/** Pre-compile check --------------------------------------------------------*/
#if ((FLASH_CONFIG_START_ADDR % FLASH_PAGE_SIZE) || ((FLASH_CONFIG_END_ADDR + 1) % FLASH_PAGE_SIZE))
    #error "Please check the address"
#endif

/** Code ---------------------------------------------------------------------*/

void FlashConfig_GetInfo(void)
{
    StoreFlash_GetInfo(&oFlashConfigObject);
}

void FlashConfig_RestoreData(void)
{
    StoreFlash_RestoreData(&oFlashConfigObject);
    // 恢复成功，更新其他相关变量
    if (mStoreFlash_RestoreIsSuccess(&oFlashConfigObject))
    {
        printf("Flash restore is success!\n\n");
    }
    else
    {
        if (!mStoreFlash_DataIsFound(&oFlashConfigObject))
        {
            printf("Flash data is not found!\n\n");
        }
        else if (mStoreFlash_CrcIsError(&oFlashConfigObject))
        {
            printf("Flash data is crc error!\n\n");
        }
        else
        {
            printf("Flash function error!\n\n");
        }
    }
}

void FlashConfig_SaveData(void)
{
    static uint8_t u8Step = 0;

    if (mTimeFlag_Is1secPassed())
    {
        switch (u8Step)
        {
            case 0:
            u8Step++;
            break;
            case 1:
            u8Config1 = 8;
            u16Config2 = 256;
            printf("Flash -> new config data: %d, %d\n", u8Config1, u16Config2);
            u8Step++;
            break;
            case 2:
            u8Config1 = 0;
            u16Config2 = 0;
            FlashConfig_GetInfo();
            FlashConfig_RestoreData();
            printf("Flash config data: %d, %d: \n\n", u8Config1, u16Config2);
            u8Step++;
            break;
            default:
            break;
        }
    }

    // 数据改变则存储
    if (StoreFlash_DataIsChanged(&oFlashConfigObject))
    {
        StoreFlash_SaveData(&oFlashConfigObject);
    }
}

/******************************** End of file *********************************/
