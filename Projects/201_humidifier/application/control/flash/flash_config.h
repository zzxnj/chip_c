/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __FLASH_CONFIG_H
#define __FLASH_CONFIG_H

/** Includes -----------------------------------------------------------------*/

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void FlashConfig_GetInfo(void);
void FlashConfig_RestoreData(void);
void FlashConfig_SaveData(void);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // FLASH_CONFIG
/******************************** End of file *********************************/
