/**
 * @author haina.z@163.com
 * @brief flash application
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "flash_apl.h"
#include "flash_config.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void Flash_GetFlashInfo(void)
{
    FlashConfig_GetInfo();
}

void Flash_RestoreData(void)
{
    FlashConfig_RestoreData();
}

void Flash_SaveDataToFlash(void)
{
    FlashConfig_SaveData();
}

/******************************** End of file *********************************/
