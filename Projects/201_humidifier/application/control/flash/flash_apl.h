/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __FLASH_APL_H
#define __FLASH_APL_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "bsp_mcu.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/**
 * Type definition, value range: [0x01 ~ 0xFE]
 */
#define FLASH_TYPE_CONFIG                       (0x01) // 配置参数

/**
 * Address definition, 1KByte
 */
#define FLASH_CONFIG_START_ADDR                 (BSP_FLASH_STORAGE_ADDRESS)
#define FLASH_CONFIG_END_ADDR                   (FLASH_CONFIG_START_ADDR + FLASH_PAGE_SIZE - 1)

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void Flash_GetFlashInfo(void);
void Flash_RestoreData(void);
void Flash_SaveDataToFlash(void);

/** Macros (#define) ---------------------------------------------------------*/
#define mFlashApl_CalculateBlocks(start, end, size) (((end)+1-(start)) / (size))

/**
 * Block number
 */
#define FLASH_CONFIG_BLOCKS                     mFlashApl_CalculateBlocks(FLASH_CONFIG_START_ADDR, FLASH_CONFIG_END_ADDR, FLASH_PAGE_SIZE)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // FLASH_APL
/******************************** End of file *********************************/
