
/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __APPLICATION_H
#define __APPLICATION_H

/** Includes -----------------------------------------------------------------*/

/* Include applications */
#include "version/version.h"
#include "variant/variant.h"

#include "digital_input/digital_input.h"
#include "sensor/sensor.h"
#include "error_handle/error_handle.h"
#include "run_mode/run_mode.h"
#include "flash/flash_apl.h"
#include "flash/flash_license1.h"
#include "flash/flash_power_fail.h"
#include "flash/flash_config.h"
#include "flash/flash_run_data.h"
#include "mb.h"
#include "mbutils.h"
#include "freemodbus/apl/modbus_apl.h"

#include "control.h"
#include "model/model_elec_heat.h"
#include "model/model_electrode.h"
#include "model/model_micromist.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus ///<use C compiler
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus ///<end extern c
}
#endif

#endif // APPLICATION
/******************************** End of file *********************************/
