/*!
 * @file        main.c
 *
 * @brief       Main program body
 *
 * @version     V1.0.0
 *
 * @date        2022-12-01
 *
 * @attention
 *
 *  Copyright (C) 2020-2022 Geehy Semiconductor
 *
 *  You may not use this file except in compliance with the
 *  GEEHY COPYRIGHT NOTICE (GEEHY SOFTWARE PACKAGE LICENSE).
 *
 *  The program is only for reference, which is distributed in the hope
 *  that it will be useful and instructional for customers to develop
 *  their software. Unless required by applicable law or agreed to in
 *  writing, the program is distributed on an "AS IS" BASIS, WITHOUT
 *  ANY WARRANTY OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the GEEHY SOFTWARE PACKAGE LICENSE for the governing permissions
 *  and limitations under the License.
 */

/* Includes */
#include "public_defs.h"
#include "prj_config.h"
#include "time/time_flag.h"

#include "board.h"
#include "main.h"

#include "test_math.h"
#include "test_xstring.h"

#include "test_csp_crc32.h"

#include "test_bit.h"
#include "test_crc.h"
#include "test_des.h"
#include "test_digital.h"
#include "test_filter.h"

#include "flash/flash_apl.h"

/** @addtogroup Examples
  @{
*/

/*!
 * @brief       Main program
 *
 * @param       None
 *
 * @retval      None
 *
 */
int main(void)
{
    // Board support package
    Bsp_Systick_Init();
    Bsp_Usart_Init();
    Bsp_LED_Init();

    // common验证
    TestMath_Perform();
    TestXString_Perform();

    // csp验证
    TestCspCrc32_Perform();

    // modules验证
    TestBit_Perform();
    TestCrc_Perform();
    TestDes_Perform();
    TestDigital_Perform1();
    TestFilter_Perform();

    // 恢复存储的数据
    Flash_GetFlashInfo();
    Flash_RestoreData();

    // Time flag
    TimeFlag_ResetTimeFlag();

    while (1)
    {
        // Time flag
        TimeFlag_GenerateTimeFlag();

        // modules验证
        TestDigital_Perform2();

        // 存储数据
        Flash_SaveDataToFlash();

        // LED indication
        if (mTimeFlag_Is1secPassed())
        {
            printf("Time: %d\n", (uint16_t)u32TimeFlag_FreeUpCount);
            Bsp_Gpio_LEDToggle();
        }
    }
}

/**@} end of group Examples */
