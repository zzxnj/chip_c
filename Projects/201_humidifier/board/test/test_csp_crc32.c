/**
 * @author haina.z@163.com
 * @brief test csp crc32
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "math/math.h"
#include "csp_crc32.h"
#include "test_csp_crc32.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static const uint32_t tlBuffer1[] = {0x11223344, 0x55667788};

/** Static function declarations (prototypes) --------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestCspCrc32_Perform(void)
{
    uint32_t u32Crc1, u32Crc2;
    uint8_t u8Cnt;

    CspCrc32_Init();

    // 一次性计算, CRC-32/MPEG-2
    u32Crc1 = CspCrc32_Calculate((uint32_t *)tlBuffer1, ARRAY_SIZE(tlBuffer1));

    // 打印原始数据和CRC结果
    printf("Csp CRC32 original data (hex): ");
    for (u8Cnt = 0; u8Cnt < ARRAY_SIZE(tlBuffer1); u8Cnt++ )
    {
        printf("%X ", tlBuffer1[u8Cnt]);
    }
    printf("-> 0x%X ", u32Crc1);

    CRC_ResetDATA();
    u32Crc2 = CspCrc32_Update((uint32_t *)tlBuffer1, ARRAY_SIZE(tlBuffer1));
    printf("-> 0x%X\n\n", u32Crc2);

    CspCrc32_Deinit();
}

/******************************** End of file *********************************/
