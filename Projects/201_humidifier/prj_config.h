
/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __PRJ_CONFIG_H
#define __PRJ_CONFIG_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "math/math_lib.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/
/* Bsp Configuration */
#define BSP_SYSTICK_FREQUENCY                               1000ul // 1ms
#define SYSTICK_PERIOD_uS                                   (1000000ul / BSP_SYSTICK_FREQUENCY)

/* Modules Configuration */
/* Time flag, use 16bits timer */
#define MODULE_TIMEFLAG_TICKS_OF_100MS                      (uint16_t)((uint32_t)BSP_SYSTICK_FREQUENCY * 100000ul / 1000000ul) // ticks of 100ms

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // PRJ_CONFIG
/******************************** End of file *********************************/
