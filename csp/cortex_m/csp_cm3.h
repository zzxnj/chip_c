/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CSP_CM3_H
#define __CSP_CM3_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** Pre-compile check --------------------------------------------------------*/
#ifndef __CHIP
    #error "Please include me in the chip header file!"
#endif
#if !(defined(__CORTEX_M) && (__CORTEX_M == 0x03U))
    #error "Please check the chip core!"
#endif

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

// Enable SysTick Interrupt
__STATIC_INLINE void CspSystick_EnableINT(void)
{
    SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
}

// Disable SysTick Interrupt
__STATIC_INLINE void CspSystick_DisableINT(void)
{
    SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
}

// Enable SysTick
__STATIC_INLINE void CspSystick_EnableTick(void)
{
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}

// Disable SysTick
__STATIC_INLINE void CspSystick_DisableTick(void)
{
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}

// Disable all interrupts in NVIC
#define mCspNvic_DisableAllIrqs() \
do { \
    uint8_t i; \
    for (i = 0; i < 8; i++) \
    { \
        NVIC->ICER[i] = 0xFFFFFFFF; \
    } \
} while(0)

// clear all interrupt's request in NVIC
#define mCspNvic_ClrAllIrqsRqt() \
do { \
    uint8_t i; \
    for (i = 0; i < 8; i++) \
    { \
        NVIC->ICPR[i] = 0xFFFFFFFF; \
    } \
} while(0)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CSP_CM3
/******************************** End of file *********************************/
