/**
 * @author haina.z@163.com
 * @brief Cortex-Mx
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "bsp_mcu.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

/**
 * @brief 检查向量表是否正确
 * 
 * @param u32VectorAddress 向量表的地址
 * @return true 向量表正确
 * @return false 
 */
bool Check_VectorTable(uint32_t u32VectorAddress)
{
    // Should be at least 0x100 aligned
    if (u32VectorAddress & 0xFF)
    {
        return false;
    }

    // 全FF或是全0
    if ((*(volatile uint32_t *)(u32VectorAddress + 0) == 0xFFFFFFFF)
    || (*(volatile uint32_t *)(u32VectorAddress + 4) == 0xFFFFFFFF)
    || (*(volatile uint32_t *)(u32VectorAddress + 0) == 0x0)
    || (*(volatile uint32_t *)(u32VectorAddress + 4) == 0x0)
    )
    {
        return false;
    }

    return true;
}

/**
 * @brief 按照向量表的定义进行跳转 Cortex-Mx
 * 
 * @param u32VectorAddress 向量表的地址
 */
void Jump_With_VectorTable(uint32_t u32VectorAddress)
{
    // Set Stack Pointer
    __set_MSP(*(volatile uint32_t *)(u32VectorAddress + 0));
    // Program Entry
    ((pfnVoid)(*(volatile uint32_t *)(u32VectorAddress + 4)))();
}

/******************************** End of file *********************************/
