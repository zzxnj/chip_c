/**
 * @author haina.z@163.com
 * @brief Chip crc32, CRC-32/MPEG-2
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "apm32f10x_rcm.h"
#include "apm32f10x_crc.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void CspCrc32_Init(void)
{
    /* Enable CRC clock */
    RCM_EnableAHBPeriphClock(RCM_AHB_PERIPH_CRC);
}

void CspCrc32_Deinit(void)
{
    RCM_DisableAHBPeriphClock(RCM_AHB_PERIPH_CRC);
}

uint32_t CspCrc32_Calculate(uint32_t *pBuf, uint32_t u32Length)
{
    CRC_ResetDATA();
    return CRC_CalculateBlockCRC(pBuf, u32Length);
}

uint32_t CspCrc32_Update(uint32_t *pBuf, uint32_t u32Length)
{
    return CRC_CalculateBlockCRC(pBuf, u32Length);
}

/******************************** End of file *********************************/
