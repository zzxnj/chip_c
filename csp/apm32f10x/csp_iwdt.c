/**
 * @author haina.z@163.com
 * @brief Chip iwdt
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "apm32f10x_iwdt.h"

/** Macros and constants -----------------------------------------------------*/

/**
 * Use LSICLK ≈ 40KHz (30KHz ~ 50KHz)
 * Default configuration, IWDT_DIVIDER_32, 250
 * 40K -> 25us * 32 * 250 = 200ms (160ms ~ 267ms)
 */

#ifndef CSP_IWDT_DIVIDER
#define CSP_IWDT_DIVIDER                        IWDT_DIVIDER_32
#endif

#ifndef CSP_IWDT_RELOAD
#define CSP_IWDT_RELOAD                         250
#endif

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/
#if ((CSP_IWDT_DIVIDER != IWDT_DIVIDER_4) \
    && (CSP_IWDT_DIVIDER != IWDT_DIVIDER_8) \
    && (CSP_IWDT_DIVIDER != IWDT_DIVIDER_16) \
    && (CSP_IWDT_DIVIDER != IWDT_DIVIDER_32) \
    && (CSP_IWDT_DIVIDER != IWDT_DIVIDER_64) \
    && (CSP_IWDT_DIVIDER != IWDT_DIVIDER_128) \
    && (CSP_IWDT_DIVIDER != IWDT_DIVIDER_256) \
)
    #error "CSP_IWDT_DIVIDER setting error!"
#endif

#if (CSP_IWDT_RELOAD > 0xFFF)
    #error "CSP_IWDT_RELOAD must be 12 bits!"
#endif

/** Code ---------------------------------------------------------------------*/

void CspIwdt_Init(void)
{
    IWDT_EnableWriteAccess();
    IWDT_ConfigDivider(CSP_IWDT_DIVIDER);
    IWDT_ConfigReload(CSP_IWDT_RELOAD);
    IWDT_Enable();
}

/******************************** End of file *********************************/
