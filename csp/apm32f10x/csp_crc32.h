/**
 * @author haina.z@163.com
 * @brief header file, CRC-32/MPEG-2.
 * @version V1.0
 */

#ifndef __CSP_CRC32_H
#define __CSP_CRC32_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "apm32f10x_crc.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/**
 * @brief Crc32 init & deinit
 */
void CspCrc32_Init(void);
void CspCrc32_Deinit(void);

/**
 * @brief Reset value and calculate buffer data
 * @return uint32_t crc32
 */
uint32_t CspCrc32_Calculate(uint32_t *, uint32_t);

/**
 * @brief Only calculate and update crc32
 * @return uint32_t crc32
 */
uint32_t CspCrc32_Update(uint32_t *, uint32_t);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CSP_CRC32
/******************************** End of file *********************************/
