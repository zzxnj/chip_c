/**
 * @author haina.z@163.com
 * @brief header file, APM32F103xB.
 * @version V1.0
 */

#ifndef __CSP_APM32F103XB_H
#define __CSP_APM32F103XB_H
#define __CHIP

/** Includes -----------------------------------------------------------------*/
#include "apm32f10x.h"
#include "../cortex_m/csp_interrupt.h"
#include "../cortex_m/csp_cm3.h"

/** Pre-compile check --------------------------------------------------------*/
#ifndef __BSP_MCU_H
    #error "Please include me in the bsp_mcu.h file!"
#endif

#ifndef APM32F10X_MD
    #error "Usually, predefined APM32F10X_MD is required in the compiler!"
#endif

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/**
 * APM32F103xB:
 * Flash 128Kbyte 0x20000 (0x08000000 ~ 0x801FFFF)
 * SRAM 20KByte 0x5000 (0x20000000 ~ 0x20004FFF)
 */

#define CSP_FLASH_ADDRESS                       (0x08000000ul)
#define CSP_FLASH_SIZE                          (0x20000ul)

#define CSP_SRAM_ADDRESS                        (0x20000000ul)
#define CSP_SRAM_SIZE                           (0x5000ul)

// ----------------------------------------------------------------------
// APM32F103x4/x6/x8/xB, The size of the main flash page is 1K Bytes
#define FLASH_PAGE_SIZE                         (0x400)
#define FLASH_ALIGN_SIZE                        (0x04)

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

/** Includes -----------------------------------------------------------------*/
#include "csp_utils.h"

#endif // CSP_APM32F103XB
/******************************** End of file *********************************/
