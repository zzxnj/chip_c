/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CSP_RST_H
#define __CSP_RST_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "apm32f10x_rcm.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

// Is software reset
#define mCspRst_IsSwReset()                     (RCM->CSTS_B.SWRSTFLG)

// Is IWDT reset
#define mCspRst_IsIwdtReset()                   (RCM->CSTS_B.IWDTRSTFLG)

// Clear all reset flags
#define mCspRst_ClearResetFlags()               RCM_ClearStatusFlag()

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CSP_RST
/******************************** End of file *********************************/
