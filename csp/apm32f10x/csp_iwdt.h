/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CSP_IWDT_H
#define __CSP_IWDT_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "apm32f10x_iwdt.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void CspIwdt_Init(void);

/** Macros (#define) ---------------------------------------------------------*/
#define mWatchdog_Init()                        do { CspIwdt_Init(); } while(0)
#define mWatchdog_Trigger()                     do { IWDT_Refresh(); } while(0)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CSP_IWDT
/******************************** End of file *********************************/
