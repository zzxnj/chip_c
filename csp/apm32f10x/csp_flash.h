/**
 * @author haina.z@163.com
 * @brief header file, for apm32f10x series.
 * @version V1.0
 */

#ifndef __CSP_FLASH_H
#define __CSP_FLASH_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/
// Invalid value, 0xFF
#define CSP_FLASH_INVALID_VALUE                 0xFFu

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void CspFlash_Read(uint32_t u32Address, uint8_t * pBuffer, uint16_t u16Length);
bool CspFlash_Write(uint32_t u32Address, uint8_t * pBuffer, uint16_t u16Length);
bool CspFlash_Erase(uint32_t u32Address);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CSP_FLASH
/******************************** End of file *********************************/
