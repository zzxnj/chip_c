/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CSP_UTILS_H
#define __CSP_UTILS_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** Pre-compile check --------------------------------------------------------*/
#ifndef __CHIP
    #error "Please include me in the chip header file!"
#endif

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/
// UID information
#define UID_BASE                                0x1FFFF7E8UL        /*!< Unique device ID register base address */
#define UID_BYTES                               (12u)               // 96 bits

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/
// little-endian
#define mLow(w)                                 mGetOffset0Byte(&(w))
#define mHigh(w)                                mGetOffset1Byte(&(w))

#define mLL(l)                                  mGetOffset0Byte(&(l))
#define mLH(l)                                  mGetOffset1Byte(&(l))
#define mHL(l)                                  mGetOffset2Byte(&(l))
#define mHH(l)                                  mGetOffset3Byte(&(l))

// Get UID
__STATIC_INLINE void Csp_GetUID(uint8_t * pBuf)
{
    uint8_t i;
    for (i = 0; i < UID_BYTES; i++)
    {
        pBuf[i] = *(__I uint8_t *)(UID_BASE + i);
    }
}

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CSP_UTILS
/******************************** End of file *********************************/
