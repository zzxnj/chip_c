/**
 * @author haina.z@163.com
 * @brief digital
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "../time/time_flag.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

/**
 * @brief 数字量滤波
 * 
 * @param bDigital 数字量，可以是某个条件
 * @param pCnt 计数器
 * @param u8Num 需要维持的次数
 * @return true 数字量已达到指定的次数
 * @return false 
 */
bool Digital_Debounce(const bool bDigital, uint8_t *pCnt, const uint8_t u8Num)
{
    if (bDigital)
    {
        if (*pCnt < 0xFF)
        {
            (*pCnt)++;
        }
        return (*pCnt >= u8Num) ? true : false;
    }
    else
    {
        *pCnt = 0;
        return false;
    }
}

/**
 * @brief 数字量维持
 * 
 * @param bDigital 数字量，可以是某个条件，或是某个按键状态
 * @param pCnt 计数器
 * @param u8Time 以100ms为单位，需要保持的时间
 * @return true 数字量已维持到指定的时间
 * @return false 
 */
bool Digital_Keep(const bool bDigital, uint8_t *pCnt, const uint8_t u8Time)
{
    if (bDigital)
    {
        if (mTimeFlag_Is100msPassed() && (*pCnt < 0xFF))
        {
            (*pCnt)++;
        }
        return (*pCnt >= u8Time) ? true : false;
    }
    else
    {
        *pCnt = 0;
        return false;
    }
}

/******************************** End of file *********************************/
