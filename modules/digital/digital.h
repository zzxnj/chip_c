/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __DIGITAL_H
#define __DIGITAL_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/**
 * @brief 数字量滤波
 * 
 * @param bDigital 数字量，可以是某个条件
 * @param pCnt 计数器
 * @param u8Num 需要维持的次数
 * @return true 数字量已达到指定的次数
 * @return false 
 */
bool Digital_Debounce(const bool bDigital, uint8_t *pCnt, const uint8_t u8Num);

/**
 * @brief 数字量维持
 * 
 * @param bDigital 数字量，可以是某个条件，或是某个按键状态
 * @param pCnt 计数器
 * @param u8Time 以100ms为单位，需要保持的时间
 * @return true 数字量已维持到指定的时间
 * @return false 
 */
bool Digital_Keep(const bool bDigital, uint8_t *pCnt, const uint8_t u8Time);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // DIGITAL
/******************************** End of file *********************************/
