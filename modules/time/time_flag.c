/**
 * @author haina.z@163.com
 * @brief time flag.
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "time_flag.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static uint16_t u16TimeFlag_LastValue;
static uint8_t u8TimeFlag_1sCnt;

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/
uint32_t u32TimeFlag_FreeUpCount;
uint8_t u8TimeFlag_Flags;

/** Code ---------------------------------------------------------------------*/

/**
 * @brief Reset time flag
 */
void TimeFlag_ResetTimeFlag(void)
{
    u16TimeFlag_LastValue = (uint16_t)u32TimeFlag_FreeUpCount;
    u8TimeFlag_1sCnt = 0;
}

/**
 * @brief Check 100ms passed
 * @return true: 100ms passed
 * @return false 
 */
static bool TimeFlag_privGenerateTime100ms(void)
{
    uint16_t u16CurrentCount = (uint16_t)u32TimeFlag_FreeUpCount;

    if (((uint16_t)(u16CurrentCount - u16TimeFlag_LastValue)) >= (uint16_t)(MODULE_TIMEFLAG_TICKS_OF_100MS))
    {
        u16TimeFlag_LastValue += ((uint16_t)(MODULE_TIMEFLAG_TICKS_OF_100MS));
        return true;
    }

    return false;
}

/**
 * @brief Time flag called in main loop
 */
void TimeFlag_GenerateTimeFlag(void)
{
    u8TimeFlag_Flags = 0;

    if (TimeFlag_privGenerateTime100ms())
    {
        u8TimeFlag_1sCnt++;
        if (u8TimeFlag_1sCnt >= 10)
        {
            u8TimeFlag_1sCnt = 0;
            u8TimeFlag_Flags = (bmTimeFlag100ms | bmTimeFlag1sec);
        }
        else
        {
            u8TimeFlag_Flags = bmTimeFlag100ms;
        }
    }
}

/******************************** End of file *********************************/
