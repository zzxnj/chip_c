/**
 * @author haina.z@163.com
 * @brief header file, Time flag [100ms, 1sec].
 * @version V1.0
 */

#ifndef __TIME_FLAG_H
#define __TIME_FLAG_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/
/* time flag mask */
#define bmTimeFlag100ms                                     0x01
#define bmTimeFlag1sec                                      0x08

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/
/**
 * Usage:
 *      u32TimeFlag_FreeUpCount++, in periodic timer;
 *      MODULE_TIMEFLAG_TICKS_OF_100MS, defined the relationship between the time of periodic calls and 100ms.
 */
extern uint32_t u32TimeFlag_FreeUpCount;
extern uint8_t u8TimeFlag_Flags;

/** Function declarations (prototypes) ---------------------------------------*/
/**
 * @brief Reset TimeFlag, only need to be called once
 */
void TimeFlag_ResetTimeFlag(void);
/**
 * @brief Generate TimeFlag, usually called in the main loop
 */
void TimeFlag_GenerateTimeFlag(void);

/** Macros (#define) ---------------------------------------------------------*/
/* Time flags */
#define mTimeFlag_Is100msPassed()                           (u8TimeFlag_Flags & bmTimeFlag100ms)
#define mTimeFlag_Is1secPassed()                            (u8TimeFlag_Flags & bmTimeFlag1sec)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // TIME_FLAG
/******************************** End of file *********************************/
