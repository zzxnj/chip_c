/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __BIT_ACCESS_H
#define __BIT_ACCESS_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/
extern const uint8_t u8BitAccessByteMask[BITS_PER_BYTE];
extern const uint16_t u16BitAccessWordMask[BITS_PER_WORD];

bool_t BitAccess_ByteFieldBitIsSet(uint8_t *pBuf, uint16_t u16Offset);
void BitAccess_ByteFieldSetBit(uint8_t *pBuf, uint16_t u16Offset);
void BitAccess_ByteFieldClrBit(uint8_t *pBuf, uint16_t u16Offset);

bool_t BitAccess_WordFieldBitIsSet(uint16_t *pBuf, uint16_t u16Offset);
void BitAccess_WordFieldSetBit(uint16_t *pBuf, uint16_t u16Offset);
void BitAccess_WordFieldClrBit(uint16_t *pBuf, uint16_t u16Offset);

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/
#define mBitAccess_GetByteMask(index)                       u8BitAccessByteMask[index]
#define mBitAccess_GetWordMask(index)                       u16BitAccessWordMask[index]

/* bits number to bytes */
#define mBitFieldGetBytes(bitsNumber)                       (((bitsNumber) + BITS_PER_BYTE - 1) / BITS_PER_BYTE)

/* bits number to words */
#define mBitFieldGetWords(bitsNumber)                       (((bitsNumber) + BITS_PER_WORD - 1) / BITS_PER_WORD)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // BIT_ACCESS
/******************************** End of file *********************************/
