/**
 * @author haina.z@163.com
 * @brief bit_access.
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "bit_access.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
 
/** Private variable definitions (static) ------------------------------------*/
 
/** Function declarations (prototypes) ---------------------------------------*/
 
/** Public variable definitions ----------------------------------------------*/
const uint8_t u8BitAccessByteMask[BITS_PER_BYTE] = {
    0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80
};
const uint16_t u16BitAccessWordMask[BITS_PER_WORD] = {
    0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
    0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000
};

/** Code ---------------------------------------------------------------------*/

/**
 * @brief  位操作 判断数组中某个位是否置位
 * @param  pBuf buffer
 * @param  u16Offset 偏移
 * @retval 是否置位
 */
bool_t BitAccess_ByteFieldBitIsSet(uint8_t *pBuf, uint16_t u16Offset)
{
    return (pBuf[u16Offset / BITS_PER_BYTE] & mBitAccess_GetByteMask(u16Offset % BITS_PER_BYTE));
}

/**
 * @brief  位操作 置位
 * @param  pBuf buffer
 * @param  u16Offset 偏移
 * @retval 无
 */
void BitAccess_ByteFieldSetBit(uint8_t *pBuf, uint16_t u16Offset)
{
    pBuf[u16Offset / BITS_PER_BYTE] |= mBitAccess_GetByteMask(u16Offset % BITS_PER_BYTE);
}

/**
 * @brief  位操作 清零
 * @param  pBuf buffer
 * @param  u16Offset 偏移
 * @retval 无
 */
void BitAccess_ByteFieldClrBit(uint8_t *pBuf, uint16_t u16Offset)
{
    pBuf[u16Offset / BITS_PER_BYTE] &= ~mBitAccess_GetByteMask(u16Offset % BITS_PER_BYTE);
}

/**
 * @brief  位操作 判断数组中某个位是否置位
 * @param  pBuf buffer
 * @param  u16Offset 偏移
 * @retval 是否置位
 */
bool_t BitAccess_WordFieldBitIsSet(uint16_t *pBuf, uint16_t u16Offset)
{
    return (pBuf[u16Offset / BITS_PER_WORD] & mBitAccess_GetWordMask(u16Offset % BITS_PER_WORD));
}

/**
 * @brief  位操作 置位
 * @param  pBuf buffer
 * @param  u16Offset 偏移
 * @retval 无
 */
void BitAccess_WordFieldSetBit(uint16_t *pBuf, uint16_t u16Offset)
{
    pBuf[u16Offset / BITS_PER_WORD] |= mBitAccess_GetWordMask(u16Offset % BITS_PER_WORD);
}

/**
 * @brief  位操作 清零
 * @param  pBuf buffer
 * @param  u16Offset 偏移
 * @retval 无
 */
void BitAccess_WordFieldClrBit(uint16_t *pBuf, uint16_t u16Offset)
{
    pBuf[u16Offset / BITS_PER_WORD] &= ~mBitAccess_GetWordMask(u16Offset % BITS_PER_WORD);
}

/******************************** End of file *********************************/
