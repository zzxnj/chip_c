/**
 * @author haina.z@163.com
 * @brief Railway repeater communication protocol
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "com_rr_dll.h"

/** Macros and constants -----------------------------------------------------*/
// 接收发送超时时间 unit: 1ms
#define COM_RR_RECV_OVERTIME                                30
#define COM_RR_SEND_OVERTIME                                30
#define COM_RR_FRAME_SPACE                                  50

// 转义字符
#define COM_RR_DMC                                          0x7c        // 报文头尾
#define COM_RR_CHG                                          0x5c        // 转义字符
#define COM_RR_RPL1                                         0x7b
#define COM_RR_RPL2                                         0x5b

// 转义标志
#define COM_RR_T_CH                                         0x01        // 发送转义处理
#define COM_RR_R_CH                                         0x02        // 接收转义处理

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static uint16_t twRRCrcTable[256] =
{
    0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
    0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
    0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
    0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
    0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
    0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
    0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
    0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
    0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
    0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
    0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
    0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
    0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
    0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
    0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
    0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
    0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
    0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
    0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
    0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
    0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
    0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
    0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
    0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
    0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
    0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
    0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
    0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
    0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
    0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
    0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
    0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Macros and constants -----------------------------------------------------*/
// 检查接收缓冲区是否溢出
#define mComRRDll_BufIsOverflow(len)                        ((len) >= COM_RR_BUF_SIZE)

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

//================================================================================
// 函数名:      uint16_t ComRRDll_CalculateCRC(uint8_t * ptr, uint16_t len)
// 功能描述:    生成CRC16
// 参数:        数据指针 长度
// 返回值:      无
//================================================================================
uint16_t ComRRDll_CalculateCRC(uint8_t * ptr, uint16_t len)
{
    uint16_t crc = 0;
    uint8_t dat;

    while(len-- != 0)
    {
        dat = (uint8_t)(crc / 256);
        crc <<= 8;
        crc ^= twRRCrcTable[dat ^ *ptr];
        ptr++;
    }

    return(crc);
}

//================================================================================
// 函数名:      void ComRRDll_TimeUpdate(void)
// 功能描述:    通讯时钟管理
// 参数:        串口
// 返回值:      无
//================================================================================
void ComRRDll_TimeUpdate(COM_RR_INST * pCurCom)
{
    // if (pCurCom == (COM_RR_INST *)NULL){ return; }

    if (pCurCom->recv.timer > 0) // 接收
    {
        pCurCom->recv.timer--;
    }

    if (pCurCom->send.spacetime > 0)
    {
        pCurCom->send.spacetime--;
    }
    if (pCurCom->send.timer > 0) // 发送
    {
        pCurCom->send.timer--;
        if (pCurCom->send.timer == 0x0)
        {
            pCurCom->send.FactCnt = 0x0;
            pCurCom->send.spacetime = COM_RR_FRAME_SPACE;
        }
    }
}

//================================================================================
// 函数名:      void ComRRDll_RecvEvent(COM_RR_INST * pCurCom, uint8_t dat)
// 功能描述:    铁标接收事件
// 参数:        串口 数据
// 返回值:      无
//================================================================================
void ComRRDll_RecvEvent(COM_RR_INST * pCurCom, uint8_t dat)
{
    // if (pCurCom == (COM_RR_INST *)NULL){ return; }

    if (pCurCom->recv.Valid > 0){ return; } // 有数据要处理
    pCurCom->recv.MemPtr = pCurCom->recv.buf;

    if (pCurCom->recv.timer == 0x0)
    {
        pCurCom->recv.step = 0x0; // 复位接收
    }

    switch (pCurCom->recv.step)
    {
        //--------------------------------------------------
        case 0:        // 起始头
        default:
        {
            if (dat == COM_RR_DMC) // 铁标起始头
            {
                *(pCurCom->recv.MemPtr) = COM_RR_DMC;
                
                pCurCom->recv.FactCnt = 1;
                pCurCom->recv.step = 0x01;
                
                pCurCom->state &= (~COM_RR_R_CH); // 转义字符用(清除)
            }
        }
        break;

        //--------------------------------------------------
        case 1:
        {
            if (dat == COM_RR_DMC) // 重播起始头，维持在本状态机下
            {
                *(pCurCom->recv.MemPtr) = COM_RR_DMC;
                
                pCurCom->recv.FactCnt = 1;
                
                pCurCom->state &= (~COM_RR_R_CH); // 转义字符用(清除)
            }
            else if (pCurCom->recv.FactCnt != 1)
            {
                pCurCom->recv.step = 0x0; // 出错
            }
            else
            {
                *(pCurCom->recv.MemPtr + pCurCom->recv.FactCnt) = dat;
                
                pCurCom->recv.FactCnt++;
                pCurCom->recv.step++;
                
                if (dat == COM_RR_CHG) // 转义标志
                {
                    pCurCom->state |= COM_RR_R_CH;
                }
                else
                {
                    pCurCom->state &= (~COM_RR_R_CH);
                }
            }
        }
        break;

        //--------------------------------------------------
        case 2:        // 数据
        {
            if (mComRRDll_BufIsOverflow(pCurCom->recv.FactCnt))
            {
                pCurCom->recv.step = 0x0; // 溢出则复位缓冲区
            }
            else if (pCurCom->state & COM_RR_R_CH) // 转义
            {
                if (dat == COM_RR_RPL1)
                {
                    *(pCurCom->recv.MemPtr + pCurCom->recv.FactCnt - 1) = COM_RR_DMC;
                }
                
                pCurCom->state &= (~COM_RR_R_CH); // 转义字符用(清除)
            }
            else // 无转义
            {
                *(pCurCom->recv.MemPtr + pCurCom->recv.FactCnt) = dat;
                pCurCom->recv.FactCnt++;
                
                if (dat == COM_RR_CHG){ pCurCom->state |= COM_RR_R_CH; } // 转义标志
                else if (dat == COM_RR_DMC) // 铁标结束
                {
                    pCurCom->recv.Len = pCurCom->recv.FactCnt;
                    pCurCom->recv.Valid = 1;
                    
                    pCurCom->recv.step = 0x0;
                }
            }
        }
        break;

        //--------------------------------------------------
    }

    pCurCom->recv.timer = COM_RR_RECV_OVERTIME;      // 接收超时
}

//================================================================================
// 函数名:      uint8_t ComRRDll_StartSend(COM_RR_INST * pCurCom, uint8_t * Buf, uint16_t Len, uint8_t * pdat)
// 功能描述:    通讯模块启动发送
// 参数:        通讯控制实体,数据指针,长度
// 返回值:      是否可以发送
//================================================================================
uint8_t ComRRDll_StartSend(COM_RR_INST * pCurCom, uint8_t * Buf, uint16_t Len, uint8_t * pdat)
{
    if ((Buf == (uint8_t *)NULL) || (Len == 0x0)){ return FALSE; }
    pCurCom->state &= (~COM_RR_T_CH); // 清发送转义

    if (pCurCom->send.buf != Buf)
    {
        // Len = mMin(Len, COM_RR_BUF_SIZE);
        // memory_copy(pCurCom->send.buf, Buf, Len);
    }
    pCurCom->send.MemPtr = pCurCom->send.buf;
    pCurCom->send.FactCnt = Len;
    
    if ((pCurCom->send.FactCnt > 0) && (pCurCom->send.MemPtr[0] == COM_RR_DMC))
    {
        pCurCom->send.FactCnt--;
        
        *pdat = *pCurCom->send.MemPtr++; // 发送数据
        
        if (pCurCom->send.FactCnt == 0x0)
        {
            pCurCom->send.timer = 0x0;
            pCurCom->send.spacetime = COM_RR_FRAME_SPACE;
        }
        else
        {
            pCurCom->send.timer = COM_RR_SEND_OVERTIME; // 发送超时
        }

        return TRUE;
    }

    return FALSE;
}

//================================================================================
// 函数名:      uint8_t ComRRDll_SendEvent(COM_RR_INST * pCurCom, uint8_t * pdat)
// 功能描述:    铁标发送事件
// 参数:        串口 数据
// 返回值:      是否有数据发送
//================================================================================
uint8_t ComRRDll_SendEvent(COM_RR_INST * pCurCom, uint8_t * pdat)
{
    if (pCurCom->send.FactCnt > 0)
    {
        if (pCurCom->state & COM_RR_T_CH)
        {
            pCurCom->send.FactCnt--;
            *pdat = *pCurCom->send.MemPtr++; // 发送数据
            
            pCurCom->state &= (~COM_RR_T_CH);
        }
        else
        {
            if (pCurCom->send.FactCnt == 0x1)
            {
                pCurCom->send.FactCnt--;
                *pdat = *pCurCom->send.MemPtr++; // 发送数据
            }
            else if (*pCurCom->send.MemPtr == COM_RR_DMC)
            {
                *pdat = COM_RR_CHG; // 发送数据
                *pCurCom->send.MemPtr = COM_RR_RPL1;
                
                pCurCom->state |= COM_RR_T_CH;
            }
            else if (*pCurCom->send.MemPtr == COM_RR_CHG)
            {
                *pdat = COM_RR_CHG; // 发送数据
                *pCurCom->send.MemPtr = COM_RR_RPL2;
                
                pCurCom->state |= COM_RR_T_CH;
            }
            else
            {
                pCurCom->send.FactCnt--;
                *pdat = *pCurCom->send.MemPtr++; // 发送数据
            }
        }
        
        if (pCurCom->send.FactCnt == 0x0)
        {
            pCurCom->send.timer = 0x0;
            pCurCom->send.spacetime = COM_RR_FRAME_SPACE;
        }
        else
        {
            pCurCom->send.timer = COM_RR_SEND_OVERTIME; // 发送超时
        }

        return TRUE;
    }
    else
    {
        pCurCom->send.spacetime = COM_RR_FRAME_SPACE;
    }
    
    return FALSE;
}

/**
 * @brief 增加头尾和CRC
 * 
 * @param DatLen 数据长度
 * @return uint16_t 返回总长度
 */
uint16_t ComRRDll_BuildSendFrame(COM_RR_INST * pCurCom, uint16_t DatLen)
{
    uint16_t u16CRC;

    pCurCom->send.buf[0] = COM_RR_DMC;
    u16CRC = ComRRDll_CalculateCRC(&(pCurCom->send.buf[1]), DatLen);
    pCurCom->send.buf[DatLen + 1] = u16CRC >> 8;
    pCurCom->send.buf[DatLen + 2] = u16CRC >> 0;
    pCurCom->send.buf[DatLen + 3] = COM_RR_DMC;

    return (DatLen + 4);
}

/******************************** End of file *********************************/
