/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __DES_H
#define __DES_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/**
 * @brief DES加密和解密函数
 * 
 * @param pDesDatIn 待加密或待解密的数据 8Bytes
 * @param pDesKey 密钥 8Bytes
 * @param pDesDatOut 加密或解密之后的数据 8Bytes
 */
void Des_Encrypt(uint8_t *pDesDatIn, uint8_t *pDesKey, uint8_t *pDesDatOut);
void Des_Decrypt(uint8_t *pDesDatIn, uint8_t *pDesKey, uint8_t *pDesDatOut);

/** Inline function definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // DES
/******************************** End of file *********************************/
