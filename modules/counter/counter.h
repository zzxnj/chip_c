/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __COUNTER_H
#define __COUNTER_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
typedef struct
{
    uint8_t u8Counter;
} Counter8Bits_t;
typedef struct
{
    uint16_t u16Counter;
} Counter16Bits_t;
typedef struct
{
    uint32_t u32Counter;
} Counter32Bits_t;

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void Counter8_DownTick(Counter8Bits_t * this);
void Counter16_DownTick(Counter16Bits_t * this);
void Counter32_DownTick(Counter32Bits_t * this);

/** Macros (#define) ---------------------------------------------------------*/
// 赋初值
#define mCounter8_SetCounter(this, u8Data)                  do { (this)->u8Counter = (u8Data); } while(0)
// 减计数
#define mCounter8_DownTick(this) \
do { \
    if ((this)->u8Counter > 0) { (this)->u8Counter--; } \
} while(0)
// 已计到零
#define mCounter8_DownIsExpired(this)                       ((this)->u8Counter == 0)

// 赋初值
#define mCounter16_SetCounter(this, u16Data)                do { (this)->u16Counter = (u16Data); } while(0)
// 减计数
#define mCounter16_DownTick(this) \
do { \
    if ((this)->u16Counter > 0) { (this)->u16Counter--; } \
} while(0)
// 已计到零
#define mCounter16_DownIsExpired(this)                      ((this)->u16Counter == 0)

// 赋初值
#define mCounter32_SetCounter(this, u32Data)                do { (this)->u32Counter = (u32Data); } while(0)
// 减计数
#define mCounter32_DownTick(this) \
do { \
    if ((this)->u32Counter > 0) { (this)->u32Counter--; } \
} while(0)
// 已计到零
#define mCounter32_DownIsExpired(this)                      ((this)->u32Counter == 0)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // COUNTER
/******************************** End of file *********************************/
