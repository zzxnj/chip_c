/**
 * @author haina.z@163.com
 * @brief counter module
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "counter.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
 
/** Private variable definitions (static) ------------------------------------*/
 
/** Function declarations (prototypes) ---------------------------------------*/
 
/** Public variable definitions ----------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

/**
 * @brief  减计数
 * @param  this 数据指针
 * @retval None
 */
void Counter8_DownTick(Counter8Bits_t * this)
{
    if (this->u8Counter > 0)
    {
        this->u8Counter--;
    }
}

/**
 * @brief  减计数
 * @param  this 数据指针
 * @retval None
 */
void Counter16_DownTick(Counter16Bits_t * this)
{
    if (this->u16Counter > 0)
    {
        this->u16Counter--;
    }
}

/**
 * @brief  减计数
 * @param  this 数据指针
 * @retval None
 */
void Counter32_DownTick(Counter32Bits_t * this)
{
    if (this->u32Counter > 0)
    {
        this->u32Counter--;
    }
}

/******************************** End of file *********************************/
