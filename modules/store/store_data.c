/**
 * @author haina.z@163.com
 * @brief store data
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "store_data.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

/**
 * real value is changed
 */
bool StoreData_IsChanged(const StoreData_t * const pList, uint8_t * const pBuf, const uint8_t u8Num)
{
    uint8_t u8Index,u8Tmp;
    for (u8Index = 0; u8Index < u8Num; u8Index++)
    {
        u8Tmp = pBuf[u8Index] ^ *(pList[u8Index].pByte);
        if (u8Tmp & pList[u8Index].u8Mask)
        {
            return true;
        }
    }
    return false;
}

/**
 * restore real value from buffer
 */
void StoreData_Restore(const StoreData_t * const pList, uint8_t * const pBuf, const uint8_t u8Num)
{
    uint8_t u8Index;
    for (u8Index = 0; u8Index < u8Num; u8Index++)
    {
        *(pList[u8Index].pByte) &= ~(pList[u8Index].u8Mask);
        *(pList[u8Index].pByte) |= (pBuf[u8Index] & pList[u8Index].u8Mask);
    }
}

/**
 * copy real value to buffer
 */
void StoreData_Update(const StoreData_t * const pList, uint8_t * const pBuf, const uint8_t u8Num)
{
    uint8_t u8Index;
    for (u8Index = 0; u8Index < u8Num; u8Index++)
    {
        pBuf[u8Index] = *(pList[u8Index].pByte) & pList[u8Index].u8Mask;
    }
}

/******************************** End of file *********************************/
