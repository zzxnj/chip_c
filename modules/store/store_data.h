/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __STORE_DATA_H
#define __STORE_DATA_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
typedef struct
{
    uint8_t * pByte;
    uint8_t u8Mask;
} StoreData_t;

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
/**
 * @brief real value is changed
 */
bool StoreData_IsChanged(const StoreData_t * const pList, uint8_t * const pBuf, const uint8_t u8Num);
/**
 * @brief restore real value from buffer
 */
void StoreData_Restore(const StoreData_t * const pList, uint8_t * const pBuf, const uint8_t u8Num);
/**
 * @brief copy real value to buffer
 */
void StoreData_Update(const StoreData_t * const pList, uint8_t * const pBuf, const uint8_t u8Num);

/** Macros (#define) ---------------------------------------------------------*/
#define mStoreData_Compare(pStoreData, mirror_data)         ((*((pStoreData)->pByte) ^ (mirror_data)) & (pStoreData)->u8Mask)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // STORE_DATA
/******************************** End of file *********************************/
