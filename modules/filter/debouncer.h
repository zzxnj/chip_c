/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __DEBOUNCER_H
#define __DEBOUNCER_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
typedef struct
{
    uint8_t u8OK;
    uint8_t u8NOK;
    uint8_t u8Cnt;
} DebouncerByte_t;

typedef struct
{
    uint16_t u16OK;
    uint16_t u16NOK;
    uint8_t u8Cnt;
} DebouncerWord_t;

typedef struct
{
    uint32_t u32OK;
    uint32_t u32NOK;
    uint8_t u8Cnt;
} DebouncerLong_t;

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/**
 * @brief Applying debounce
 * 
 * @param pDebouncer Current debouncer
 * @param u8New New input
 * @param u8Debounce Number of debounce
 */
void Debouncer_ByteApply(DebouncerByte_t * const pDebouncer, const uint8_t u8New, const uint8_t u8Debounce);
void Debouncer_WordApply(DebouncerWord_t * const pDebouncer, const uint16_t u16New, const uint8_t u8Debounce);
void Debouncer_LongApply(DebouncerLong_t * const pDebouncer, const uint32_t u32New, const uint8_t u8Debounce);

/** Macros (#define) ---------------------------------------------------------*/
/**
 * Initialize debouncer and get debounce value
*/
#define mDebouncer_ByteInit(this, init) do { \
    (this)->u8OK = (this)->u8NOK = (init); \
} while(0)
#define mDebouncer_ByteGetValue(this) ((this)->u8OK)

/**
 * Initialize debouncer and get debounce value
*/
#define mDebouncer_WordInit(this, init) do { \
    (this)->u16OK = (this)->u16NOK = (init); \
} while(0)
#define mDebouncer_WordGetValue(this) ((this)->u16OK)

/**
 * Initialize debouncer and get debounce value
*/
#define mDebouncer_LongInit(this, init) do { \
    (this)->u32OK = (this)->u32NOK = (init); \
} while(0)
#define mDebouncer_LongGetValue(this) ((this)->u32OK)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // DEBOUNCER
/******************************** End of file *********************************/
