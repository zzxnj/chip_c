/**
 * @author haina.z@163.com
 * @brief debouncer
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "debouncer.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/
 
/** Private variable definitions (static) ------------------------------------*/
 
/** Function declarations (prototypes) ---------------------------------------*/
 
/** Public variable definitions ----------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void Debouncer_ByteApply(DebouncerByte_t * const pDebouncer, const uint8_t u8New, const uint8_t u8Debounce)
{
    if (u8New != pDebouncer->u8NOK)
    {
        pDebouncer->u8NOK = u8New;
        pDebouncer->u8Cnt = 0;
    }
    else if (pDebouncer->u8Cnt < 0xFF)
    {
        pDebouncer->u8Cnt++;
    }

    if (pDebouncer->u8Cnt >= u8Debounce)
    {
        pDebouncer->u8OK = pDebouncer->u8NOK;
    }
}

void Debouncer_WordApply(DebouncerWord_t * const pDebouncer, const uint16_t u16New, const uint8_t u8Debounce)
{
    if (u16New != pDebouncer->u16NOK)
    {
        pDebouncer->u16NOK = u16New;
        pDebouncer->u8Cnt = 0;
    }
    else if (pDebouncer->u8Cnt < 0xFF)
    {
        pDebouncer->u8Cnt++;
    }

    if (pDebouncer->u8Cnt >= u8Debounce)
    {
        pDebouncer->u16OK = pDebouncer->u16NOK;
    }
}

void Debouncer_LongApply(DebouncerLong_t * const pDebouncer, const uint32_t u32New, const uint8_t u8Debounce)
{
    if (u32New != pDebouncer->u32NOK)
    {
        pDebouncer->u32NOK = u32New;
        pDebouncer->u8Cnt = 0;
    }
    else if (pDebouncer->u8Cnt < 0xFF)
    {
        pDebouncer->u8Cnt++;
    }

    if (pDebouncer->u8Cnt >= u8Debounce)
    {
        pDebouncer->u32OK = pDebouncer->u32NOK;
    }
}

/******************************** End of file *********************************/
