/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CRC32_H
#define __CRC32_H

/** Includes -----------------------------------------------------------------*/
#include <stdint.h>

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/
#define CRC32_ETHERNET_POLY                     0x4C11DB7

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void CRC32_Init(uint32_t u32Poly);
uint32_t CRC32_Calculate(uint32_t u32Crc, const void *pInput, uint32_t u32Len);

/** Inline function definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/
/**
 * usage:
 * uint32_t crc;
 * CRC32_Init(CRC32_ETHERNET_POLY);
 * 
 * crc = 0xFFFFFFFF;
 * crc = CRC32_Calculate(crc, "1234567890", 10);
 * crc ^= 0xFFFFFFFF;
*/
#define mCRC32_EthernetCalculate(pInput, u32Len) (CRC32_Calculate(0xFFFFFFFF, pInput, u32Len) ^ 0xFFFFFFFF)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CRC32
/******************************** End of file *********************************/
