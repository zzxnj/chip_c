/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CRC16_H
#define __CRC16_H

/** Includes -----------------------------------------------------------------*/
#include <stdint.h>

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
uint16_t CRC16Modbus_CalculateWithPoly(const uint8_t *pDat, uint16_t u16Len);
uint16_t CRC16Modbus_CalculateWithTable(const uint8_t *pDat, uint16_t u16Len);

uint16_t CRC16XModem_CalculateWithPoly(const uint8_t *pDat, uint16_t u16Len);
uint16_t CRC16XModem_CalculateWithTable(const uint8_t *pDat, uint16_t u16Len);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CRC16
/******************************** End of file *********************************/
