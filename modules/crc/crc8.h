/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __CRC8_H
#define __CRC8_H

/** Includes -----------------------------------------------------------------*/
#include <stdint.h>

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void CRC8_Init(uint8_t *pCrc8);

void CRC8_UpdateWithPoly(uint8_t *pCrc8, uint8_t u8New);
void CRC8_UpdateWithTable(uint8_t *pCrc8, uint8_t u8New);

uint8_t CRC8_CalculateWithPoly(const uint8_t *pDat, uint16_t u16Len);
uint8_t CRC8_CalculateWithTable(const uint8_t *pDat, uint16_t u16Len);

/** Macros (#define) ---------------------------------------------------------*/
#define CRC8_Update                             CRC8_UpdateWithPoly
#define CRC8_Calculate                          CRC8_CalculateWithPoly

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // CRC8
/******************************** End of file *********************************/
