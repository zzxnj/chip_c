/**
 * @author haina.z@163.com
 * @brief test digital
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "digital/digital.h"
#include "test_digital.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Static function declarations (prototypes) --------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestDigital_Perform1(void)
{
    static uint8_t u8Cnt = 0;

    // 调用两次后，状态返回true
    printf("Digital debounce, 1/2 -> %d\n", Digital_Debounce(true, &u8Cnt, 2));
    printf("2/2 -> %d\n\n", Digital_Debounce(true, &u8Cnt, 2));
}

void TestDigital_Perform2(void)
{
    static uint8_t u8Cnt1 = 0, u8Cnt2 = 0xff;
    bool bResult;

    bResult = Digital_Keep(true, &u8Cnt1, 50);

    // u8Cnt1 变化后，打印一次
    if (u8Cnt2 != u8Cnt1)
    {
        u8Cnt2 = u8Cnt1;
        printf("Digital keep -> %d %d\n", bResult, u8Cnt2);
    }
}

/******************************** End of file *********************************/
