/**
 * @author haina.z@163.com
 * @brief test des
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "algorithm\des\des.h"
#include "test_des.h"

/** Macros and constants -----------------------------------------------------*/
#define DES_KEY "12345678"

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static uint8_t u8DesKey[] = DES_KEY;
static uint8_t u8DesDatIn[8] = {205, 0, 9, 29, 2, 82, 81, 108};
static uint8_t u8DesDatOut[8];

/** Static function declarations (prototypes) --------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestDes_Perform(void)
{
    uint8_t i;

    // 执行加密算法
    Des_Encrypt(u8DesDatIn, u8DesKey, u8DesDatOut);

    printf("Des original data:");
    for (i = 0; i < 8; i++)
    {
        printf(" %d", u8DesDatIn[i]);
        u8DesDatIn[i] = 0; // 原始数据清零
    }
    printf("\n Key:");
    for (i = 0; i < 8; i++)
    {
        printf(" %d", u8DesKey[i]);
    }
    printf("\n After encryption:");
    for (i = 0; i < 8; i++)
    {
        printf(" %d", u8DesDatOut[i]);
    }

    // 执行解密算法
    Des_Decrypt(u8DesDatOut, u8DesKey, u8DesDatIn);
    printf("\n After decryption:");
    for (i = 0; i < 8; i++)
    {
        printf(" %d", u8DesDatIn[i]);
    }
    printf("\n\n");
}

/******************************** End of file *********************************/
