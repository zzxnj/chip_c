/**
 * @author haina.z@163.com
 * @brief test bit
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "bit_access\bit_access.h"
#include "test_bit.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static uint8_t trBuffer1[mBitFieldGetBytes(40)];
static uint16_t twBuffer2[mBitFieldGetWords(45)];

/** Static function declarations (prototypes) --------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestBit_Perform(void)
{
    BitAccess_ByteFieldSetBit(trBuffer1, 30);
    BitAccess_ByteFieldClrBit(trBuffer1, 35);

    printf("Bit access is set %d\n", BitAccess_ByteFieldBitIsSet(trBuffer1, 30));
    printf("Bit access is clear %d\n\n", BitAccess_ByteFieldBitIsSet(trBuffer1, 35));

    BitAccess_WordFieldSetBit(twBuffer2, 18);
    BitAccess_WordFieldClrBit(twBuffer2, 28);

    printf("Bit access is set %d\n", BitAccess_WordFieldBitIsSet(twBuffer2, 18));
    printf("Bit access is clear %d\n\n", BitAccess_WordFieldBitIsSet(twBuffer2, 28));
}

/******************************** End of file *********************************/
