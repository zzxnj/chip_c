/**
 * @author haina.z@163.com
 * @brief test xstring functions
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "math/math.h"
#include "xstring\xstring.h"
#include "test_xstring.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static const struct
{
    const char *s;
    const char *k;
} oStr[] = {
    // 可按下列格式增加数据
    {"uuabcdefghiuuusijfoasidfjsoadf",      "uuu"},
    {"hosifuoasjfso293sjdi203",             "abc"},
};

// 可增删下面的字符串
static const char *sStr1[] = {
    "b73f28c0",
    "2882b3e",
    "12a34f9e812b",
    "23b 837"
};
static uint8_t trBuffer1[5];

/** Static function declarations (prototypes) --------------------------------*/
static void TestXString_privStringToHex(const char *sString);
static void TestXString_privHexStringToValue(const char *sString, uint8_t u8Number);

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestXString_Perform(void)
{
    uint8_t u8Cnt;

    // 搜索字符串
    for (u8Cnt = 0; u8Cnt < ARRAY_SIZE(oStr); u8Cnt++)
    {
        printf("Search %s in %s, ", oStr[u8Cnt].k, oStr[u8Cnt].s);
        printf("Position: %d\n\n", xString_SearchKey(oStr[u8Cnt].s, oStr[u8Cnt].k));
    }

    // 字符串转数值
    for (u8Cnt = 0; u8Cnt < ARRAY_SIZE(sStr1); u8Cnt++)
    {
        TestXString_privStringToHex(sStr1[u8Cnt]);
        TestXString_privHexStringToValue(sStr1[u8Cnt], 2); // 获取2个字节的数据
    }
}

static void TestXString_privStringToHex(const char *sString)
{
    uint8_t u8Cnt;

    // 原始数据
    printf("String to hex: %s, ", sString);
    // 转换的结果
    printf("Result: %d, ", xString_StringToHex(sString, trBuffer1, sizeof(trBuffer1)));
    for (u8Cnt = 0; u8Cnt < sizeof(trBuffer1); u8Cnt++)
    {
        printf("0x%X ", trBuffer1[u8Cnt]);
        trBuffer1[u8Cnt] = 0;
    }
    printf("\n\n");
}

static void TestXString_privHexStringToValue(const char *sString, uint8_t u8Number)
{
    uint8_t u8Cnt;

    // 原始数据
    printf("Hex string to value: %s, ", sString);
    // 转换的结果 bool型
    printf("Result: %d, ", xString_HexStringToValue(sString, trBuffer1, u8Number));
    for (u8Cnt = 0; u8Cnt < sizeof(trBuffer1); u8Cnt++)
    {
        printf("0x%X ", trBuffer1[u8Cnt]);
        trBuffer1[u8Cnt] = 0;
    }
    printf("\n\n");
}

/******************************** End of file *********************************/
