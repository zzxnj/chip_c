/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __TEST_CRC_H
#define __TEST_CRC_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void TestCrc_Perform(void);

/** Inline function definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // TEST_CRC
/******************************** End of file *********************************/
