/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __TEST_DIGITAL_H
#define __TEST_DIGITAL_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void TestDigital_Perform1(void);
void TestDigital_Perform2(void);

/** Inline function definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // TEST_DIGITAL
/******************************** End of file *********************************/
