/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __TEST_DES_H
#define __TEST_DES_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void TestDes_Perform(void);

/** Inline function definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // TEST_DES
/******************************** End of file *********************************/
