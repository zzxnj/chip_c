/**
 * @author haina.z@163.com
 * @brief test_filter
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "filter\debouncer.h"
#include "test_filter.h"

/** Macros and constants -----------------------------------------------------*/
#define TEST_VALUE_A1 0x12
#define TEST_VALUE_A2 0x90

#define TEST_VALUE_B1 0x102a
#define TEST_VALUE_B2 0x70b4

#define TEST_VALUE_C1 0x1043342a
#define TEST_VALUE_C2 0x021b4b4a

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
static DebouncerByte_t oDebouncerByte;
static DebouncerWord_t oDebouncerWord;
static DebouncerLong_t oDebouncerLong;

/** Static function declarations (prototypes) --------------------------------*/
static void TestFilter_privDebouncer(void);

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestFilter_Perform(void)
{
    TestFilter_privDebouncer();
}

static void TestFilter_privDebouncer(void)
{
    // Byte based debouncer
    mDebouncer_ByteInit(&oDebouncerByte, TEST_VALUE_A1);
    printf("Debouncer data: %d, ", TEST_VALUE_A1);
    Debouncer_ByteApply(&oDebouncerByte, TEST_VALUE_A2, 1);
    printf("apply once: %d, ", mDebouncer_ByteGetValue(&oDebouncerByte));
    Debouncer_ByteApply(&oDebouncerByte, TEST_VALUE_A2, 1);
    printf("apply again: %d\n\n", mDebouncer_ByteGetValue(&oDebouncerByte));

    // Word based debouncer
    mDebouncer_WordInit(&oDebouncerWord, TEST_VALUE_B1);
    printf("Debouncer data: %d, ", TEST_VALUE_B1);
    Debouncer_WordApply(&oDebouncerWord, TEST_VALUE_B2, 1);
    printf("apply once: %d, ", mDebouncer_WordGetValue(&oDebouncerWord));
    Debouncer_WordApply(&oDebouncerWord, TEST_VALUE_B2, 1);
    printf("apply again: %d\n\n", mDebouncer_WordGetValue(&oDebouncerWord));

    // Long based debouncer
    mDebouncer_LongInit(&oDebouncerLong, TEST_VALUE_C1);
    printf("Debouncer data: %d, ", TEST_VALUE_C1);
    Debouncer_LongApply(&oDebouncerLong, TEST_VALUE_C2, 1);
    printf("apply once: %d, ", mDebouncer_LongGetValue(&oDebouncerLong));
    Debouncer_LongApply(&oDebouncerLong, TEST_VALUE_C2, 1);
    printf("apply again: %d\n\n", mDebouncer_LongGetValue(&oDebouncerLong));
}

/******************************** End of file *********************************/
