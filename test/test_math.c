/**
 * @author haina.z@163.com
 * @brief test math functions
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "math/math.h"
#include "math/math_lib.h"
#include "test_math.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/
// 原始数据，可修改后，重新运行计算
static uint16_t trBuffer1[] = { 0x0123, 0x4567, 0x89ab, 0xcdef };

/** Static function declarations (prototypes) --------------------------------*/
static void TestMath_privAverage1(uint16_t * pBuf, uint8_t u8Len);

/** Public variable definitions ----------------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void TestMath_Perform(void)
{
    TestMath_privAverage1(trBuffer1, ARRAY_SIZE(trBuffer1));
}

static void TestMath_privAverage1(uint16_t * pBuf, uint8_t u8Len)
{
    Avg1Word_t oAvg;
    uint8_t u8Cnt;

    mMath_Avg1Init(&oAvg);

    for (u8Cnt = 0; u8Cnt < u8Len; u8Cnt++)
    {
        // 原始数据
        printf("%d ", pBuf[u8Cnt]);
        if (Math_Average1Apply(&oAvg, pBuf[u8Cnt], u8Len))
        {
            // 输出平均值
            printf("-> Average: %d\n\n", mMath_GetAvg1Value(&oAvg));
        }
    }
}

/******************************** End of file *********************************/
