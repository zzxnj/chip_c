/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __BSP_SYSTICK_H
#define __BSP_SYSTICK_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void Bsp_Systick_Init(void);
void Bsp_Systick_Deinit(void);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // BSP_SYSTICK
/******************************** End of file *********************************/
