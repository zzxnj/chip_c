/**
 * @author haina.z@163.com
 * @brief bsp gpio
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "apm32f10x_rcm.h"
#include "apm32f10x_gpio.h"
#include "bsp_gpio.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

/**
 * @brief 配置LED
 */
void Bsp_LED_Init(void)
{
    GPIO_Config_T GPIO_configStruct;

    /* Enable the Clock */
    RCM_EnableAPB2PeriphClock(LED_GPIO_CLK);

    // LED
    GPIO_configStruct.mode = GPIO_MODE_OUT_PP;
    GPIO_configStruct.speed = GPIO_SPEED_50MHz;

    GPIO_configStruct.pin = LED_GPIO_PIN;
    GPIO_Config(LED_GPIO_PORT, &GPIO_configStruct);
}

void Bsp_LED_Deinit(void)
{
    GPIO_Config_T GPIO_configStruct;

    GPIO_ConfigStructInit(&GPIO_configStruct);
    GPIO_configStruct.pin = LED_GPIO_PIN;
    GPIO_Config(LED_GPIO_PORT, &GPIO_configStruct);

    RCM_DisableAPB2PeriphClock(LED_GPIO_CLK);
}

/******************************** End of file *********************************/
