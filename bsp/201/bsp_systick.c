/**
 * @author haina.z@163.com
 * @brief bsp systick 
 * @version V1.0
 */

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "bsp_mcu.h"

/** Macros and constants -----------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** Private variable definitions (static) ------------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Public variable definitions ----------------------------------------------*/

/** Pre-compile check --------------------------------------------------------*/

/** Code ---------------------------------------------------------------------*/

void Bsp_Systick_Init(void)
{
    /* SystemCoreClock / 1000    1ms中断一次
     * SystemCoreClock / 10000   100us中断一次
     */
    if (SysTick_Config(SystemCoreClock / BSP_SYSTICK_FREQUENCY))
    {
        /* Capture error */
        while (1);
    }
}

void Bsp_Systick_Deinit(void)
{
    CspSystick_DisableTick();
    CspSystick_DisableINT();
}

/******************************** End of file *********************************/
