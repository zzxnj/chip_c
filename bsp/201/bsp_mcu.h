/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __BSP_MCU_H
#define __BSP_MCU_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "prj_config.h"
#include "csp_apm32f103xb.h"

#include "apm32f10x_rcm.h"
#include "apm32f10x_misc.h"
#include "apm32f10x_gpio.h"
#include "apm32f10x_usart.h"
#include "apm32f10x_tmr.h"

#include "csp_crc32.h"
#include "csp_flash.h"
#include "csp_iwdt.h"
#include "csp_rst.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

#define BOOT_CODE_SIZE                          (0x1000ul) // 4K Bytes
#define BOOT_RAM_OFFSET                         (0x1000ul) // 4K
#define BSP_FLASH_STORAGE_SIZE                  (0x2000ul) // 8K Bytes

// Boot and Flash storage allocation
#define BOOT_ROM_ADDRESS                        CSP_FLASH_ADDRESS
#define BOOT_APP_ADDRESS                        (CSP_FLASH_ADDRESS + BOOT_CODE_SIZE)
#define BOOT_INFO_ADDRESS                       (CSP_FLASH_ADDRESS + CSP_FLASH_SIZE - BSP_FLASH_STORAGE_SIZE - FLASH_PAGE_SIZE) // Information, last 1K Bytes
#define BOOT_APP_SIZE                           (BOOT_INFO_ADDRESS - BOOT_APP_ADDRESS)

#define BOOT_EXCHANGE_ADDRESS                   0x20000000
#define BOOT_RAM_ADDRESS                        (CSP_SRAM_ADDRESS + BOOT_RAM_OFFSET)

#define BSP_FLASH_STORAGE_ADDRESS               (CSP_FLASH_ADDRESS + CSP_FLASH_SIZE - BSP_FLASH_STORAGE_SIZE)

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // BSP_MCU
/******************************** End of file *********************************/
