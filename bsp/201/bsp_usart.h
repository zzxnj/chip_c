/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __BSP_USART_H
#define __BSP_USART_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "apm32f10x_gpio.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/
#define RS485_COM                               USART2
// 波特率
// #define BSP_USART_BAUDRATE                      9600
#define BSP_USART_BAUDRATE                      115200

// R/T口
#define RS485_COM_RT_PIN                        GPIO_PIN_4
#define RS485_COM_RT_GPIO_PORT                  GPIOA
#define RS485_COM_RT_GPIO_CLK                   RCM_APB2_PERIPH_GPIOA
#define mSetRS485Recv()                         { RS485_COM_RT_GPIO_PORT->BC = RS485_COM_RT_PIN; }
#define mSetRS485Send()                         { RS485_COM_RT_GPIO_PORT->BSC = RS485_COM_RT_PIN; }

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void Bsp_Usart_Init(void);
void Bsp_Usart_Deinit(void);

/** Macros (#define) ---------------------------------------------------------*/

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // BSP_USART
/******************************** End of file *********************************/
