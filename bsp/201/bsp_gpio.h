/**
 * @author haina.z@163.com
 * @brief header file.
 * @version V1.0
 */

#ifndef __BSP_GPIO_H
#define __BSP_GPIO_H

/** Includes -----------------------------------------------------------------*/
#include "public_defs.h"
#include "apm32f10x_gpio.h"

/** C++ { --------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C"
{
#endif

/** Macros (#define) ---------------------------------------------------------*/

/** Type definitions ---------------------------------------------------------*/

/** External variable declarations (extern) ----------------------------------*/

/** Function declarations (prototypes) ---------------------------------------*/
void Bsp_LED_Init(void);
void Bsp_LED_Deinit(void);

/** Macros (#define) ---------------------------------------------------------*/

// ----------------------------------------------------------------------
// LED
#define LED_GPIO_PIN                GPIO_PIN_12
#define LED_GPIO_PORT               GPIOB
#define Bsp_Gpio_LEDToggle()        do { LED_GPIO_PORT->ODATA ^= LED_GPIO_PIN; } while(0)
#define LED_GPIO_CLK                RCM_APB2_PERIPH_GPIOB
#define Bsp_TurnOffLED()            do { LED_GPIO_PORT->ODATA |= LED_GPIO_PIN; } while(0)

/** C++ } --------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // BSP_GPIO
/******************************** End of file *********************************/
